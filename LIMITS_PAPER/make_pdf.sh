#!/usr/bin/env bash
echo Downloading Paper source
curl -o body.tex https://pad.vvvvvvaria.org/pmc_aesthetics-toc-limits/export/txt
echo Downloading bibtex source
curl -o pmc_aesthetics.bib https://pad.vvvvvvaria.org/pmc_aesthetics_limits.bib/export/txt
echo Compiling LaTeX - ROUND 1
lualatex pmc_aesthetics
echo Compiling BibTeX
bibtex pmc_aesthetics 
echo Compiling LaTeX - ROUND II
lualatex pmc_aesthetics
echo "Compiling LaTeX to PDF... FINALLY!"
lualatex pmc_aesthetics
